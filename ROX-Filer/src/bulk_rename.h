/*
 * ROX-Filer, filer for the ROX desktop project
 * By Thomas Leonard, <tal197@users.sourceforge.net>.
 */

#ifndef _BULK_RENAME_H
#define _BULK_RENAME_H

void bulk_rename(const char *dir, GList *items);

#endif /* _BULK_RENAME_H */
